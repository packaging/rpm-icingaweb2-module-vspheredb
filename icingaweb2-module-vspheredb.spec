# vSphere DB Module for Icinga Web 2 | (c) 2018-2019 Icinga Development Team <info@icinga.com> | GPLv2+

%global revision 1
%global module_name vspheredb

%global icingaweb_min_version 2.6.0

Name:           icingaweb2-module-%{module_name}
Version:        1.0.4
Release:        %{revision}%{?dist}
Summary:        vSphere DB - Icinga Web 2 module
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
Source1:        %{service_name}.service
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}
%global service_name icinga-%{module_name}

%if "%{_vendor}" == "suse"
%global service_user wwwrun
%else # suse
%global service_user apache
%endif # suse

BuildRequires:  systemd-devel
Requires:       systemd

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       icingaweb2-module-director >= 1.6
Requires:       icingaweb2-module-ipl >= 0.1
Requires:       icingaweb2-module-incubator >= 0.1
Requires:       icingaweb2-module-reactbundle >= 0.4

# Compare to icingaweb2.spec
%if 0%{?rhel} == 7
%global php_scl         rh-php71
%endif

%if 0%{?php_scl:1}
%global php_scl_prefix  %{php_scl}-
%endif

%global php             %{?php_scl_prefix}php

%if 0%{?suse_version}
Requires:      %{php}-soap %{php}-posix %{php}-pcntl
%else # suse_version
Requires:      %{php}-process %{php}-soap
%endif # suse_version

%description
The easiest way to monitor a VMware vSphere environment. Configure a
connection to your VMware vCenter® or VMware ESXi™ host and you're ready to
go. This module provides a lot of context, deep insight and great oversight.
Fast drill-down possibilities, valuable hints and reports.

You'll immediately see all your Host Systems, Virtual Machines, Data Stores
and much more pop up in your Icinga Web 2 frontend. This alone is already
very helpful, but there is more.

This module:
* provides an Import Source for the Icinga Director
* hooks into the Monitoring module and shows related information next to your
  monitored Hosts
* provides Reports, helping to track down anomalies or configuration errors
* replicates the most interesting parts of your Event- and Alarm History

We currently support all VMware versions from 5.5 to 6.7.

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

install -d %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{service_name}.service

# Replace user in service unit
sed -i -e 's~^User=.*~User=%{service_user}~' %{buildroot}%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
install -d %{buildroot}%{_sbindir}
ln -sf /usr/sbin/service %{buildroot}%{_sbindir}/rc%{service_name}
%endif # suse

%clean
rm -rf %{buildroot}

%pre
%if "%{_vendor}" == "suse"
  %service_add_pre %{service_name}.service
%endif # suse

exit 0

%post
set -e

%if "%{_vendor}" == "suse"
  %service_add_post %{service_name}.service
%else # suse
%systemd_post %{service_name}.service
%endif # suse

exit 0

%preun
set -e

%if "%{_vendor}" == "suse"
  %service_del_preun %{service_name}.service
%else # suse
  %systemd_preun %{service_name}.service
%endif # suse

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%postun
set -e

%if "%{_vendor}" == "suse"
  %service_del_postun %{service_name}.service
%else # suse
  %systemd_postun_with_restart %{service_name}.service
%endif # suse

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
%{_sbindir}/rc%{service_name}
%endif # suse

%changelog
* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 1.0.4-1
- Initial package version
